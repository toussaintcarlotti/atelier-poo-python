from forme.forme2d.Cercle import Cercle
from forme.forme2d.Rectangle import Rectangle
from forme.forme2d.Ellipse import Ellipse
from forme.forme3d.Sphere import Sphere
from forme.forme3d.Cylindre import Cylindre

formes = []
j = 0
for i in range(50):
    j += 1
    if j == 1:
        formes.append(Cercle("cercle", 2))
    elif j == 2:
        formes.append(Rectangle("rectangle", 4, 6))
    elif j == 3:
        formes.append(Ellipse("ellipse", 2, 6))
    elif j == 4:
        formes.append(Sphere("sphere", 7))
    elif j == 5:
        formes.append(Cylindre("cylindre", 18, 35))
        j = 0

cercleCompare = Cercle("cercle", 15)
formes.append(Cercle("cercle", 15))
for forme in formes:
    print(str(forme) + "\nA une surface de " + str(forme.calculSurface()) + "unté²")
    if isinstance(forme, Cercle):
        if forme == cercleCompare:
            print(forme.id + " est égal à " + cercleCompare.id)