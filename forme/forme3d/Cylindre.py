from forme.forme3d.Forme3d import Forme3d


class Cylindre(Forme3d):

    def __init__(self, nom, rayon, hauteur):
        Forme3d.__init__(self, nom)
        self.rayon = rayon
        self.hauteur = hauteur

    def calculSurface(self):
        return (2 * self.PI * self.rayon * self.hauteur) + 2 * self.PI * self.rayon * self.rayon

    def calculVolume(self):
        return self.PI * self.rayon * self.rayon * self.hauteur

    def __str__(self):
        return "identifiant : " + str(self.id) + "\nRayon = " + str(self.rayon) + "\nHauteur = " + str(self.hauteur)

    def __eq__(self, other):
        result = False
        if other:
            if isinstance(other, Cylindre):
                result = other.rayon == self.rayon and self.hauteur == other.hauteur
        return result