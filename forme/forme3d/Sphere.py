from forme.forme3d.Forme3d import Forme3d


class Sphere(Forme3d):

    def __init__(self, nom, rayon):
        Forme3d.__init__(self, nom)
        self.rayon = rayon

    def calculSurface(self):
        return 4 * self.PI * self.rayon * self.rayon

    def calculVolume(self):
        return 4/3 * self.PI * self.rayon * self.rayon * self.rayon

    def __str__(self):
        return "identifiant : " + str(self.id) + "\nRayon = " + str(self.rayon)

    def __eq__(self, other):
        result = False
        if other:
            if isinstance(other, Sphere):
                result = other.rayon == self.rayon
        return result