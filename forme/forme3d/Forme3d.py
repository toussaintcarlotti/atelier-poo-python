from abc import ABC, abstractmethod
from forme.Forme import Forme


class Forme3d(Forme):

    def __init__(self, nom):
        Forme.__init__(self, nom)

    @abstractmethod
    def calculVolume(self):
        pass

