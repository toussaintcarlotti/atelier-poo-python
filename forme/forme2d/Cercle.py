from forme.forme2d.Forme2d import Forme2d

class Cercle(Forme2d):

    def __init__(self, nom, rayon):
        Forme2d.__init__(self, nom)
        self.rayon = rayon

    def calculSurface(self):
        return self.rayon * self.rayon * self.PI

    def calculPerimetre(self):
        return 2 * self.id * self.rayon

    def __str__(self):
        return "identifiant : " + str(self.id) + "\nRayon = " + str(self.rayon)

    def __eq__(self, other):
        result = False
        if other:
            if isinstance(other, Cercle):
                result = other.rayon == self.rayon
        return result
