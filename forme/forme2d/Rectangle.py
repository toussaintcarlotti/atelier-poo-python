from forme.forme2d.Forme2d import Forme2d


class Rectangle(Forme2d):

    def __init__(self, nom, longueur, largeur):
        Forme2d.__init__(self, nom)
        self.longueur = longueur
        self.largeur = largeur

    def calculSurface(self):
        return self.longueur * self.largeur

    def calculPerimetre(self):
        return self.longueur * 2 + self.largeur * 2

    def __str__(self):
        return "identifiant : " + str(self.id) + "\nLongueur = " + str(self.longueur) + "\nLargeur = " + str(self.largeur)

    def __eq__(self, other):
        result = False
        if other:
            if isinstance(other, Rectangle):
                result = other.longueur == self.longueur and other.largeur == self.largeur
        return result