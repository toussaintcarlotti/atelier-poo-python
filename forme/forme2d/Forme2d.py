from abc import ABC, abstractmethod
from forme.Forme import Forme


class Forme2d(Forme):

    def __init__(self, nom):
        Forme.__init__(self, nom)

    @abstractmethod
    def calculPerimetre(self):
        pass

