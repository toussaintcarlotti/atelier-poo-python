import math

from forme.forme2d.Forme2d import Forme2d


class Ellipse(Forme2d):

    def __init__(self, nom, a, b):
        Forme2d.__init__(self, nom)
        self.a = a
        self.b = b

    def calculSurface(self):
        return self.a * self.b * self.PI

    def calculPerimetre(self):
        return 2 * self.PI * math.sqrt((self.a * self.a + self.b * self.b)/2)

    def __str__(self):
        return "identifiant : " + str(self.id) + "\na = " + str(self.a) + "\nb = " + str(self.b)

    def __eq__(self, other):
        result = False
        if other:
            if isinstance(other, Ellipse):
                result = other.b == self.b and other.a == self.a
        return result