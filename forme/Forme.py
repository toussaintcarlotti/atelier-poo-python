import math
from abc import ABC, abstractmethod


class Forme(ABC):
    nbForme = 0
    PI = math.pi

    def __init__(self, nom):
        self.nbForme += 1
        self.id = nom + "_" + str(self.nbForme)

    @abstractmethod
    def calculSurface(self):
        pass

    def estPlusGrande(self, forme):
        return self.calculSurface() > forme.calculSurface()

    @abstractmethod
    def __eq__(self,other):
        pass

    @abstractmethod
    def __str__(self):
        pass


